
(function () {
    'use strict';

    angular
    .module('routeConfigApp')
    .controller('dashboardController', dashboardController);

    dashboardController.$inject = ['$scope', '$http', '$state', 'toastr'];
    function dashboardController($scope, $http, $state, toastr) {        
    	var init = function() {
    		$scope.dashboard = {};
    	};
    	init();
    }
})();
