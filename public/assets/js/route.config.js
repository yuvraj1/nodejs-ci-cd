var myApp = angular.module('routeConfigApp', ['ui.router', 'toastr']);

myApp.config(function($stateProvider, $urlRouterProvider) {
  var dashboardState = {
    name: 'dashboard',
    url: '/dashboard',
    data: {
      pageTitle: "Dashboard"
    },
    templateUrl: 'templates/dashboard.html',
    controller: 'dashboardController'
  }

  $urlRouterProvider.otherwise('/dashboard');
  $stateProvider.state(dashboardState);
});

myApp.run(function($rootScope, $http, $location) {

});