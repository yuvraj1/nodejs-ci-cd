var mysql = require('mysql');

var sqldb = mysql.createConnection({
	host : 'localhost',
	user : 'root',
	password : '',
	database : 'node_one'
});

sqldb.connect(function(err){
	if(err){
		console.log(err);
	} else {
		console.log("connected sql");
	}
});

module.exports =  sqldb;
