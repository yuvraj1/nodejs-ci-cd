var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
// var db  = require('./model/database');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/views/index.html');
});

app.listen(3000);
console.log("running");